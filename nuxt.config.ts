// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  app: {
    head: {
      title: '',
      titleTemplate: '%s',
      noscript: [
        {
          innerHTML:
            '<div style="display: flex; align-items: center; justify-content: center; text-align: center; width: 100%; height: 100vh; flex-direction: column; position: fixed; top: 0; left: 0; z-index: 10000; background-color: #fff; font-size: 3em; padding: 0 2em;"><h2 class="txt-danger">JavaScript is off</h2>Please enable javascript on your browser to view full site.</div>'
        }
      ],
      meta: [
        { charset: 'utf-8' },
        { name: 'viewport', content: 'width=device-width, initial-scale=1' },
        {
          name: 'google',
          content: 'notranslate'
        }
      ],
      htmlAttrs: {
        translate: 'no',
        class: 'notranslate'
      }
    }
  },
  devtools: { enabled: true },
  devServer: {
    port: parseInt(process.env.PORT ?? '') || 3000,
    host: process.env.HOST ?? 'localhost',
  },
  ssr: true,
  components: [
    { path: '~/components', pathPrefix: false }
  ],
  css: ['~/scss/style.scss'],
  pages: true,
  modules: ['@sidebase/nuxt-auth'],
  auth: {
    provider: {
      type: 'local',
      endpoints: {
        signIn: {
          path: '/api/auth/login',
          method: 'post'
        },
        signOut: {
          path: '/api/auth/logout',
          method: 'post'
        },
        signUp: {
          path: '/api/auth/user',
          method: 'post'
        },
        getSession: {
          path: '/api/auth/session',
          method: 'get'
        }
      }
    }
  }
});
